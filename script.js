$(document).ready(function () {
  //FOR MATERIALIZE
  $('.sidenav').sidenav();
  $(".dropdown-trigger").dropdown();
  $('.parallax').parallax();

  //PERSO
  $(".hide_emploi").hide();
  $(".visible_emploi").show();
  $("#et").on("click", () => {
    $(".hide_emploi").slideToggle();
    $(".visible_emploi").slideToggle();
  })
});
